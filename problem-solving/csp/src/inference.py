# -*- coding: utf-8 -*-
"""
This module defines various inference algorithms.

"""


def ac_3(csp, queue=None, removals=[]) -> bool:
    """
    Arc consistency for binary constraint networks.

    Potentially modifies one or more domains of the CSP.

    Local variables:
        queue: Set of constraint scopes (e.g. {(Xi, Xj), (Xi,Xm)})

    NOTE:
    The order of consideration is not important, so the data structure is
    really a set, but tradition calls it a queue.

    Args:
        csp     : CSP instance on which to apply constraint propagation
                  Can be a reduced version of the original domains
        queue   : Predetermined set of arcs to check for consistency;
                  Only used when interleaving constraint propagation
                  with search, in which case we may not want to run
                  AC-3 on all constraints every time
        removals: Bookeeping of the values removed from domains, in case
                  we need to restore those values when backtracking during
                  backtracking search

    Returns:
        False if an inconsistency is found and True otherwise

    """
    def revise(csp, Xi, Xj, removals) -> bool:
        """
        Perform local consistency.

        Potentially reduces domain of Xi.

        Returns:
            True iff we revise the domain of Xi

        """
        revised = False
        for x in list(csp.curr_domains[Xi]):
            if not csp.consistent(Xi, x, Xj):
                csp.prune(Xi, x, removals)
                revised = True
        return revised

    # Initialize queue with all arcs (scopes of relations).
    if not queue:
        frwrd = {(Xi, Xk) for Xi in csp.variables for Xk in csp.neighbors[Xi]}
        bkwrd = {(Xk, Xi) for Xi in csp.variables for Xk in csp.neighbors[Xi]}
        queue = frwrd | bkwrd

    # Perform local consistency.
    while len(queue) > 0:

        (Xi, Xj) = queue.pop()

        # Perform constraint propagation.
        if revise(csp, Xi, Xj, removals):

            # No solution is possible; terminate.
            if len(csp.curr_domains[Xi]) == 0:
                return False

            # Add affected scopes to queue.
            for Xk in set(csp.neighbors[Xi]) - {Xj}:
                queue.add((Xk, Xi))

    # No inconsistencies detected.
    # Does not necessarily mean that the CSP has a solution.
    return True
