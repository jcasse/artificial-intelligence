# -*- coding: utf-8 -*-
"""
This module defines unit tests.

"""
# Python built-in libraries
import unittest

# Local modules
from problems import map_coloring_csp
from solution import heuristics


class LeastConstrainingValue(unittest.TestCase):
    """Unit tests for least_constraining_value heuristic."""

    def setUp(self):
        pass

    def test_rgb(self):
        """Check correct order for example in AIMA text."""
        print()
        csp = \
            map_coloring_csp(list('RGB'),
                             'SA: WA NT Q NSW V; NT: WA Q; NSW: Q V; T: ')
        csp.assign('WA', 'R')
        csp.assign('NT', 'G')
        csp.curr_domains['WA'] = ['R']
        csp.curr_domains['NT'] = ['G']
        csp.curr_domains['SA'] = ['B']
        expect = ['-', '-', 'B']
        actual = heuristics.least_constraining_value(csp, 'Q')
        print(f'expect: {expect}')
        print(f'actual: {actual}')
        assert actual[2] == expect[2]  # R and G tied for first, B last

    def test_bgr(self):
        """Check correct order for example in AIMA text."""
        print()
        csp = \
            map_coloring_csp(list('BGR'),
                             'SA: WA NT Q NSW V; NT: WA Q; NSW: Q V; T: ')
        csp.assign('WA', 'R')
        csp.assign('NT', 'G')
        csp.curr_domains['WA'] = ['R']
        csp.curr_domains['NT'] = ['G']
        csp.curr_domains['SA'] = ['B']
        expect = ['-', '-', 'B']
        actual = heuristics.least_constraining_value(csp, 'Q')
        print(f'expect: {expect}')
        print(f'actual: {actual}')
        assert actual[2] == expect[2]  # R and G tied for first, B last

    def test_rbg(self):
        """Check correct order for example in AIMA text."""
        print()
        csp = \
            map_coloring_csp(list('RBG'),
                             'SA: WA NT Q NSW V; NT: WA Q; NSW: Q V; T: ')
        csp.assign('WA', 'R')
        csp.assign('NT', 'G')
        csp.curr_domains['WA'] = ['R']
        csp.curr_domains['NT'] = ['G']
        csp.curr_domains['SA'] = ['B']
        expect = ['-', '-', 'B']
        actual = heuristics.least_constraining_value(csp, 'Q')
        print(f'expect: {expect}')
        print(f'actual: {actual}')
        assert actual[2] == expect[2]  # R or G tied for first, B last

    def tearDown(self):
        pass
