# -*- coding: utf-8 -*-
"""
This module defines helper functions for CSP-related modules.

"""
# Python built-in libraries
from collections import defaultdict


def parse_neighbors(neighbors: str) -> dict:
    """
    Parse a compressed representation of bidirectional arcs in a network.

    Converts a string of the form 'X: Y Z; Y: Z' into a dict mapping
    nodes to neighbors. The syntax is a rnode name followed by a ':'
    followed by zero or more node names, followed by ';', repeated for
    each node name. If you say 'X: Y' you don't need 'Y: X'.

    Example:
    >>> parse_neighbors('X: Y Z; Y: Z')
    {'Y': ['X', 'Z'], 'X': ['Y', 'Z'], 'Z': ['X', 'Y']}

    Args:
        neighbors: String of the form described above

    Returns:
        mapping of nodes to list of neighbors

    """
    ret = defaultdict(list)
    specs = [spec.split(':') for spec in neighbors.split(';')]
    for (A, Aneighbors) in specs:
        A = A.strip()
        Aneighbors = Aneighbors.split()
        for B in Aneighbors:
            ret[A].append(B)
            ret[B].append(A)
        if not Aneighbors:
            ret[A] == []
    return dict(ret)
