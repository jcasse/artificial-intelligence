# -*- coding: utf-8 -*-
"""
This module defines various constraints for CSPs.

"""


def queen_constraint(A, B, a, b):
    """
    Constraint for the N-queen problem.

    Args:
        (A, B): integer representing a queen (the column where it belongs)
        (a, b): integer representing the placement of the queen in the column

    Returns:
        True if constraint is satisfied (A, B are really the same variable,
        or if they are not in the same row, down diagonal, or up diagonal),
        False otherwise

    """
    return A == B or (a != b and A + a != B + b and A - a != B - b)
