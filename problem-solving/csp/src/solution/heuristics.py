# -*- coding: utf-8 -*-
"""This module defines various heuristics."""
# Python built-in libraries
import sys

# Local modules
from utils import first, count
from inference import ac_3


###############################################################################
# Variable Selection
###############################################################################
def first_unassigned_variable(csp) -> list:
    """Variable next in the default data structure order."""
    return first([v for v in csp.variables if v not in csp.assignment()])


def mrv(csp) -> list:
    """
    Variable with minimum remaining values.

    Returns:
        Variable or None if all domains have only 1 value

    """
    ret = None
    min_count = None
    for var, domain in csp.curr_domains.items():
        if len(domain) > 1:  # unassigned variable
            if not min_count or len(domain) < min_count:
                min_count = len(domain)
                ret = var
    return ret


def highest_degree(csp) -> list:
    """
    Variable involved in the most constraints with unassigned variables.

    Local variable max_count initialized to -1 in case of variables not
    involved in any constraint. Otherwise, a variable with no constraint
    will never be selected.
    For example, Tasmania in the Australia map coloring problem.

    """
    ret = None
    max_count = -1
    assignment = csp.assignment()
    for var, neighbors in csp.neighbors.items():
        if var not in assignment and len(neighbors) > max_count:
            max_count = len(neighbors)
            ret = var
    return ret


###############################################################################
# Value Selection
###############################################################################
def default_order_domain_values(csp, var) -> list:
    """Return all values for var that aren't currently ruled out."""
    return csp.curr_domains[var]


def least_constraining_value(csp, var) -> list:
    """Return all values in order of least constraining."""
    inferences = []  # Number of inferences per value
    for val in csp.curr_domains[var]:

        removals = csp.suppose(var, val)

        # val is not possible given current domains of unassigned variables
        # (some domain was reduced to zero during forward checking)
        if not forward_checking(csp, var, val, removals):
            inferences.append((val, sys.maxsize))

        # Count number of removals (inferences) on other variables
        else:
            inferences.append((val,
                               count(True for v, _ in removals if v != var)))

        csp.restore(removals)

    # Sort values in increasing order of their inference counts
    # (i.e., least constraining value first)
    return [x[0] for x in sorted(inferences, key=lambda x: x[1])]


###############################################################################
# Inference
###############################################################################
def no_inference(*_) -> bool:
    """Perform no inference. Simply allow backtracking to continue."""
    return True


def forward_checking(csp, var, val, removals) -> bool:
    """Prune neighbor values inconsistent with var=val."""
    for B in csp.neighbors[var]:
        if B not in csp.assignment():
            for b in csp.curr_domains[B][:]:
                if not csp.constraints[var, B](var, B, val, b):
                    csp.prune(B, b, removals)
            if not csp.curr_domains[B]:
                return False
    return True


def mac(csp, var, _, removals, constraint_propagation=ac_3) -> bool:
    """Maintain arc consistency."""
    return constraint_propagation(csp,
                                  {(X, var) for X in csp.neighbors[var]},
                                  removals)
