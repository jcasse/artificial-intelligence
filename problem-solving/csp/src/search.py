# -*- coding: utf-8 -*-
"""
This module defines the backtracking search algorithm.

The variable "count" is defined in the global context so that it does not have
to be passed as an argument in the backtrack() function.

"""

# Total number of variable assignments tried during the search
count = None


def backtracking_search(csp,
                        select_unassigned_variable,
                        order_domain_values,
                        inference) -> bool:
    """
    Search for a solution to a CSP.

    Local variables:
        removals: keeps track of domain reductions in order to restore
                  them if the search backtracks

    Args:
        csp                       : problem instance
        select_unassigned_variable: heuristic: selects next variable to assign
        order_domain_values       : heuristic: determines order in which to try values
        inference                 : heuristic: chooses type of inference

    Returns:
        True if solution found, else False

    """
    def backtrack(csp) -> bool:
        """
        Recursive depth-first search maintaining only one node in memory.

        Local variables:
            removals: keeps track of domain reductions in order to restore
                      them if the search backtracks

        Args:
            csp: problem instance

        Returns:
            True if solution found, else False

        """
        print()
        print(f'domains: {csp.curr_domains}')

        var = select_unassigned_variable(csp)

        print(f'variable selected: {var}')

        # All variables assigned (domains reduced to a single value)
        if not var:
            return True

        # Try values for variable, one at a time
        for val in order_domain_values(csp, var):

            global count
            count += 1

            print(f'value selected: {val}', end=': ')

            # Value does not conflict with current assignment, proceed
            if not csp.nconflicts(var, val):

                removals = csp.suppose(var, val)

                # Inference did not detect problems with the value, proceed
                if inference(csp, var, val, removals):

                    print('assigned')
                    print(f'domain reductions by inference: {[t for t in removals if t[0] != var]}')

                    # Continue to assign next variable, recursively
                    result = backtrack(csp)

                    # Solution found
                    if result:
                        return True

                # Inference found that the value is not possible, branch pruned
                else:
                    print('inconsistency found by inference, try another value')

                csp.restore(removals)

            # Value conflicts with current assignment of other variables
            else:
                print('conflict with current assignment, try another value')

        # No values were possible for var, given current assignment
        # Backtrack and modify current assignment
        print(f'no legal values for {var} left to assign, backtrack')
        return False

    ###########################################################################
    # Start
    ###########################################################################
    global count
    count = 0
    return backtrack(csp)
