# -*- coding: utf-8 -*-
"""
This module defines various constraints for CSPs.

"""


def asymmetrical_constraint(A, B, a, b):
    """
    A constraint encoding the relation Y = X^2.

    Args:
        (A, B): tuple of variables in the constraint
                the possible values for A and B are "X" and "Y"
        (a, b): values taken by the variables

    Returns:
        True if constraint is satisfied (values are different),
        False otherwise

    """
    if A == 'X' and B == 'Y':
        return b == a * a
    if A == 'Y' and B == 'X':
        return a == b * b
    raise ValueError('Allowable values for (A, B) are (X, Y) or (Y, X)')


def different_values_constraint(A, B, a, b):
    """
    A constraint saying two neighboring variables must differ in value.

    Args:
        (A, B): tuple of variables in the constraint
        (a, b): values taken by the variables

    Returns:
        True if constraint is satisfied (values are different),
        False otherwise

    """
    return a != b
