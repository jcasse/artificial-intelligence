# -*- coding: utf-8 -*-
"""
This module implements a CSP definition.

In constraint satisfaction problems (CSPs), we can do a specific type of
inference called constraint propagation: using the constraints to reduce
the number of legal values for a variable, which in turn can reduce the legal
values for another variable, and so on.

Constraint propagation may be intertwined with search, or it may be done as a
preprocessing step, before search starts. Sometimes this preprocessing can
solve the whole problem, so no search is required at all.

The key idea is local consistency. If we treat each variable as a node in a
graph and each binary constraint as an arc, then the process of enforcing
local consistency in each part of the graph causes inconsistent values to be
eliminated throughout the graph. There are different types of local
consistency:

- Node consistency

  Node consistency tightens down the domains (unary constraints) using the
  unary constraints.

  A single variable (corresponding to a node in the CSP network) is
  node-consistent if all the values in the variable's domain satisfy the
  variable's unary constraints.

- Arc consistency

  Arc consistency tightens down the domains (unary constraints) using the
  arcs (binary constraints).

  A variable in a CSP is arc-consistent if every value in its domain satisfies
  the variable's binary constraints.

  More formally, Xi is arc-consistent with respect to another variable Xj if
  for every value in the current domain Di there is some value in the domain
  Dj that satisfies the binary constraint on the arc (Xi,Xj).

  Arc consistency can go a long way toward reducing the domains of variables,
  sometimes finding a solution (by reducing every domain to size 1) and
  sometimes finding that the CSP cannot be solved (by reducing some domain to
  size 0).

- Path consistency

  Path consistency tightens the binary constraints by using implicit
  constraints that are inferred by looking at triples of variables.

  A two-variable set {Xi,Xj} is path-consistent with respect to a third
  variable Xm if, for every assignment {Xi=a, Xj=b} consistent with the
  constraints on {Xi,Xj}, there is an assignment to Xm that satisfies the
  constraints on {Xi,Xm} and {Xm,Xj}. This is called path consistency because
  one can think of it as looking at a path from Xi to Xj with Xm in the middle.

- K-consistency

  K-consistency is the generalization of levels of consistency.
  - 1-consistency - node consistency
  - 2-consistency - arc consistency
  - 3-consistency - path consistency (for binary constraint networks)

After running a local consistency algorithm, we are left with a CSP that is
equivalent to the original CSP--they both have the same solutions--but the
local-consistent CSP will in most cases be faster to search because its
variables have smaller domains.

Note:
In practice, determining the appropriate level of consistency checking is
mostly an empirical science. It can be said practitioners commonly compute
2-consistency and less commonly 3-consistency.

Note:
It is possible to extend the notion of arc consistency to handle n-ary rather
than just binary constraints; this is called generalized arc consistency or
sometimes hyperarc consistency, depending on the author. A variable Xi is
generalized arc consistent with respect to an n-ary constraint if for every
value v in the domain of Xi there exists a tuple of values that is a member of
the constraint, has all its values taken from the domains of the corresponding
variables, and has its Xi component equal to v.
For example, if all variables have the domain {0,1,2,3}, then to make the
variable X consistent with the constraint X < Y < Z, we would have to
eliminate 2 and 3 from the domain of X because the constraint cannot be
satisfied when X is 2 or 3.
It is always possible to transform all n-ary constraints into binary ones.
Therefore, the algorithms defined here work with binary constraints. That is,
constraints between two variables.

References:
- Russell & Norvig, AIMA
- https://github.com/aimacode/aima-python/blob/61d695b37c6895902081da1f37baf645b0d2658a/csp.py

"""
# Python built-in libraries
from typing import Tuple

# Local modules
from utils import count


class CSP:
    """
    This class encodes constraint satisfaction problems.

    Properties:
        variables   : list of variables; each is atomic (e.g. int or str)
                      atomic because they're used as keys in dictionaries
        domains     : dict of {var: [possible_value, ...]} entries
        constraints : dict of {(A, B): function(A, B, a, b)}
                      returns True if the assignment A=a and B=b
                      satisfies the constraint, False otherwise
        neighbors   : dict of {var: [adjacent_var, ...]}

    The class also supports data structures and methods that can be used
    by a search function to solve the CSP. Methods and slots are
    as follows:
        suppose(self, var, val) Start accumulating inferences from assuming
                                var=val.
        assignment()            Returns current assignment
        prune(self, var, val, removals): Rules out var=val.
        nconflicts(var, val, a) Return the number of other variables that
                                conflict with var=val
        consistent(self, Xi, x, Xj): Checks whether Xi=x is consistent in
                                constraint (Xi, Xj).
        curr_domains[var]       Slot: remaining consistent values for var
                                Used by constraint propagation routines.

    A solution is a complete assignment without conflicts.

    Note:
    The backtracking search algorithm in the AIMA book keeps track of the
    "explicit" assignments made and the next unassigned variable is selected
    from this assignment state. However, here the assignment state is
    implicitly maintained by curr_domains: Any variable with a domain of one
    variable is considered assigned, even if not explicitly selected by the
    algorithm to be assigned. This has the effect that when using
    forward_checking as the inference heuristic, some inferences will not be
    made because the inferences are made when a variable is explicitly
    assigned by the algorithm.
    When using AC3 as the inference heuristic, there is no difference because
    the constraints are propagated to all the variables.

    """

    def __init__(self, variables, domains, constraints: Tuple) -> None:
        """Initialize CSP object."""
        self.variables = variables
        self.domains = domains
        self.constraints = constraints[1]
        self.neighbors = constraints[0]

        self.curr_domains = {v: list(self.domains[v]) for v in self.variables}

    def __repr__(self) -> str:
        """Define string representation of the CSP object."""
        ret = '- CSP '.ljust(80, '-') + '\n'
        ret += f'Variables:\n{self.variables}' + '\n'
        ret += f'Neighbors:\n{self.neighbors}' + '\n'
        ret += f'Domains:\n{self.domains}' + '\n'
        ret += f'Constraints:\n{self.constraints}' + '\n'
        ret += f'Current Domains:\n{self.curr_domains}' + '\n'
        ret += '-' * 80
        return ret

    def display(self) -> None:
        """Print graphic representation."""
        # Defined in sub classes, such as NQueens.

    def assignment(self) -> dict:
        return {var: val[0] for var, val in self.curr_domains.items() if len(val) == 1}

    def suppose(self, var, val):
        """Start accumulating inferences from assuming var=val."""
        removals = [(var, a) for a in self.curr_domains[var] if a != val]
        self.curr_domains[var] = [val]
        return removals

    def restore(self, removals):
        """Undo a supposition and all inferences from it."""
        for var, val in removals:
            self.curr_domains[var].append(val)

    def prune(self, var, val, removals):
        """Rule out var=val."""
        self.curr_domains[var].remove(val)
        removals.append((var, val))

    def consistent(self, Xi, x, Xj) -> bool:
        """
        Check whether Xi=x is consistent in constraint (Xi, Xj).

        Returns:
            False if no value y in domainXj allows (x,y) to satisfy the
            constraint (Xi, Xj), otherwise True

        """
        for y in self.curr_domains[Xj]:
            constraint = self.constraints[(Xi, Xj)]
            if constraint(Xi, Xj, x, y) is True:
                return True
        return False

    def nconflicts(self, Xi, x) -> int:
        """Count number of conflicts with current assignment when Xi=x."""
        def conflict(Xj):
            constraint = self.constraints[(Xi, Xj)]
            assignment = self.assignment()
            return (Xj in assignment
                    and not constraint(Xi, Xj, x, assignment[Xj]))
        return count(conflict(v) for v in self.neighbors[Xi])
