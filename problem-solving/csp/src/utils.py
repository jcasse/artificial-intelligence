# -*- coding: utf-8 -*-
"""
This module defines utility functions.

Utility functions:
- can be imported by any module
- do not have side effects

"""
# Python built-in libraries
import argparse
import textwrap

# Third-party libraries
import yaml


class UniversalDict:
    """
    A universal dict maps any key to the same value.

    We use it as the domains dict for CSPs in which all variables have
    the same domain.

    Example:
    >>> d = UniversalDict(42)
    >>> d['life']
    42

    """

    def __init__(self, value):
        self.value = value

    def __getitem__(self, key):
        return self.value

    def __repr__(self):
        return f'{{Any: {self.value!r}}}'


def first(iterable, default=None):
    """Return the first element of an iterable; or default."""
    return next(iter(iterable), default)


def count(seq):
    """Count the number of items in sequence that are interpreted as true."""
    return sum(map(bool, seq))


def read_yaml(pathname: str):
    """Load data from YAML file into dict."""
    with open(pathname) as f:
        return yaml.load(f, Loader=yaml.Loader)


def parse_cmd_line_arguments():
    """Parse command-line arguments."""
    DESCRIPTION = """CSP driver

    Driver program for CSP solver."""
    parser = argparse.ArgumentParser(
        description=DESCRIPTION,
        formatter_class=argparse.RawDescriptionHelpFormatter,
        epilog=textwrap.dedent('''\

    example usage:
        python %(prog)s -h
        python %(prog)s data/xy-csp.yaml
        python %(prog)s --preprocessing data/xy-csp.yaml
        python %(prog)s data/map-coloring-aus-csp.yaml
        python %(prog)s --inference mac data/map-coloring-aus-csp.yaml
        python %(prog)s data/8-queens-csp.yaml
    '''))
    parser.add_argument('yaml_pathname',
                        help='location of problem specification YAML file')
    parser.add_argument('--preprocessing',
                        help='do constraint propagation as '
                        'a preprocessing step',
                        action='store_true')
    parser.add_argument('--select-unassigned-variable',
                        help='heuristic for selecting variable to assign '
                        'in the search',
                        default='first_unassigned_variable',
                        dest='select_unassigned_variable')
    parser.add_argument('--order-domain-values',
                        help='heuristic for selecting order in which to '
                        'try values for the variable',
                        default='default_order_domain_values',
                        dest='order_domain_values')
    parser.add_argument('--inference',
                        help='heuristic for selecting which inference to '
                        'do in the search',
                        default='no_inference')
    return parser.parse_args()
