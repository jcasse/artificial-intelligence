# -*- coding: utf-8 -*-
"""
This module defines various problems as a CSPs.

"""
from csp import CSP
from constraints import \
     asymmetrical_constraint, \
     different_values_constraint
from helpers import parse_neighbors
from utils import UniversalDict


def xy_csp(domain) -> CSP:
    """
    Make a CSP for the problem with the relation Y = X^2.

    """
    neighbors = {'X': ['Y'], 'Y': ['X']}
    return CSP(variables=list(neighbors.keys()),
               domains=UniversalDict(domain),
               constraints=(neighbors,
                            UniversalDict(asymmetrical_constraint)))


def map_coloring_csp(colors, neighbors) -> CSP:
    """
    Make a CSP for the problem map coloring.

    The problem consists of coloring a map with different colors
    for any two adjacent regions.

    Arguments are a list of colors, and a
    dict of {region: [neighbor,...]} entries. This dict may also be
    specified as a string of the form defined by parse_neighbors.

    """
    neighbors = parse_neighbors(neighbors)
    return CSP(variables=list(neighbors.keys()),
               domains=UniversalDict(colors),
               constraints=(neighbors,
                            UniversalDict(different_values_constraint)))
