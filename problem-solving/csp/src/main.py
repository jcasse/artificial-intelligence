# -*- coding: utf-8 -*-
"""
This module implements a driver for the CSP modules.

"""
# Local modules
import problems
import inference
from solution import heuristics
from search import backtracking_search
from utils import parse_cmd_line_arguments, read_yaml


###############################################################################
# Command-line Arguments
###############################################################################
args = parse_cmd_line_arguments()

###############################################################################
# CSP Configuration
###############################################################################
config = read_yaml(args.yaml_pathname)
problem = getattr(problems, config.get('name'))
csp = problem(**config.get('args'))
print(csp)

###############################################################################
# Pre-processing
###############################################################################
if args.preprocessing:
    constraint_propagation = getattr(inference, 'ac_3')
    constraint_propagation(csp)
    print(csp)

###############################################################################
# Search
###############################################################################
print('- BACKTRACKING SEARCH '.ljust(80, '-'))
select_unassigned_variable = \
  getattr(heuristics, args.select_unassigned_variable)
order_domain_values = \
  getattr(heuristics, args.order_domain_values)
inference = \
  getattr(heuristics, args.inference)
backtracking_search(csp,
                    select_unassigned_variable,
                    order_domain_values,
                    inference)
print('-' * 80)
print(csp)
csp.display()

###############################################################################
# Summary
###############################################################################
# Import the count variable after it has been modified by the search algorithm
from search import count  # noqa: E402
print('- Heuristics '.ljust(80, '-'))
print(f'Select unassigned variable: {args.select_unassigned_variable}')
print(f'Order domain values       : {args.order_domain_values}')
print(f'inference                 : {args.inference}')
print('-' * 80)
print('- Solution '.ljust(80, '-'))
print(csp.assignment())
print('-' * 80)
print('- Performance '.ljust(80, '-'))
print(f'Total number of variable assignments tried: {count}')
print('-' * 80)
