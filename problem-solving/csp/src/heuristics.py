# -*- coding: utf-8 -*-
"""
This module defines various heuristics.

"""
from utils import first
from inference import ac_3


###############################################################################
# Variable Selection
###############################################################################
def first_unassigned_variable(csp, assignment) -> list:
    """The default variable order. """
    return first([v for v in csp.variables if v not in assignment.keys()])


def mrv(csp, _) -> list:
    """Select variable with minimum remaining values."""
    # TODO


def highest_degree_variable(csp, assignment) -> list:
    """Select variable involved in the most constraints."""
    # TODO


###############################################################################
# Value Selection
###############################################################################
def default_order_domain_values(csp, var, _) -> list:
    """Return all values for var that aren't currently ruled out."""
    return csp.curr_domains[var]


def least_constraining_value(csp, var, assignment) -> list:
    """Return all values in order of least constraining."""
    # TODO


###############################################################################
# Inference
###############################################################################
def no_inference(*_) -> bool:
    """Perform no inference. Simply allow backtracking to continue."""
    return True


def forward_checking(csp, assignment, var, val, removals) -> bool:
    """Prune neighbor values inconsistent with var=val."""
    # TODO


def mac(csp, ignore, var, _, removals, constraint_propagation=ac_3) -> bool:
    """Maintain arc consistency."""
    # TODO
